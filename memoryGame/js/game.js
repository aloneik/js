const cells = document.getElementsByClassName("cell");
const restartButton = document.getElementById("restart_button");

const imgUrls = [
    "img/image_1.jpg",
    "img/image_2.jpg",
    "img/image_3.jpg",
    "img/image_4.jpg",
    "img/image_5.jpg", 
    "img/image_6.jpg",
    "img/image_7.jpg",
    "img/image_8.jpg"
];

var activeCells = [];
var timerId1;

function distributeImages() {
    let unusedCells = [
        0,  1,  2,  3,
        4,  5,  6,  7,
        8,  9, 10, 11,
        12, 13, 14, 15
    ];

    for (i = 0; i != imgUrls.length; i++) {
        for (j = 0; j != 2; j++) {
            let cellNum = getRandomInt(0, unusedCells.length - 1);
            cells[unusedCells[cellNum]].className += " " + imgUrls[i];
            unusedCells.splice(cellNum, 1);
        }
    }
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function cellClick(cell) {
    if (activeCells.length == 0) {
        activateCell(cell);
        timerId1 = setTimeout(deactivateActiveCell, 2000);
    } else if (activeCells.length == 1) {
        activateCell(cell);
        if (activeCells[0].className == activeCells[1].className) {
            if (timerId1) {
                clearTimeout(timerId1);
            }
            setTimeout(function() {
                fullyDeactivateActiveCell();
                fullyDeactivateActiveCell();
            }, 1000);
        } else {
            setTimeout(deactivateActiveCell, 1000);
        }
    }
}

function activateCell(cell) {
    let className = cell.className;
    if (className != "cell") {
        cell.style.backgroundImage = "url(" + className.split(" ")[1] + ")";
        activeCells.push(cell);
    }
}

function deactivateActiveCell() {
    if (activeCells.length > 0) {
        activeCells.pop().style.backgroundImage = "none";
    }
}

function fullyDeactivateActiveCell() {
    if (activeCells.length > 0) {
        let cell = activeCells.pop();
        cell.className = "cell";
        cell.style.backgroundImage = "none";
        cell.style.borderColor = "white";
    }
}

function restartGame() {
    document.location.reload(true);
}

distributeImages();
for (i = 0; i != cells.length; i++) {
    cells[i].addEventListener("click", function() {
        cellClick(this);
    });
}

restartButton.addEventListener("click", restartGame);


