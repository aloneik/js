const runButton = document.getElementById('run_button');
const clearButton = document.getElementById("clear_button");

const resultDiv = document.getElementById("result");

const x1Field = document.getElementById("x1");
const x2Field = document.getElementById("x2");

const sumModifier = document.getElementById("sum");
const mulModifier = document.getElementById("mul");
const primeNodifier = document.getElementById("prime"); 

runButton.onclick = () => {
    let x1 = parseFloat(x1Field.value);
    let x2 = parseFloat(x2Field.value);

    let res = 0;
    if (Number.isNaN(x1) || Number.isNaN(x2)) {
        alert("В поля х1 и х2 должны быть введены числовые значения.");
    } else {
        if (sumModifier.checked) {
            while (x1 != x2) {
                res += x1;
                x1++;
            }
        }
        else if (mulModifier.checked) {
            res = x1;
            x1++;
            while (x1 != x2) {
                res *= x1;
                x1++;
            }
        }
        else if (primeNodifier.checked) {
            res = "";
            while (x1 != x2) {
                if (isPrime(x1))
                    res += x1.toString() + " ";
                x1++;
            }
        }

        resultDiv.innerHTML = res;
    }
}

clearButton.onclick = () => {
    x1Field.value = "";
    x2Field.value = "";
}

function isPrime(n){
    for (i = 2; i <= Math.sqrt(n); i++)
    if (n % i == 0)
        return false;
    return true;
}